<?php

namespace GPS\ReportsBundle\Tests\Unit\Utils\Calculators\Salary;

$loader = require_once __DIR__.'/../../../../../../../../app/bootstrap.php.cache';

use GPS\ReportsBundle\Entity\JobConditions;

use GPS\ReportsBundle\Utils\Calculators\Salary\UmowaODzieloConverter;
use PHPUnit_Framework_TestCase as TestCase;

/**
 * Class SalaryNetTest
 *
 * @package GPS\ReportsBundle\Tests\Entity
 */
class UmowaODzieloConverterTest extends TestCase
{

    public function setUp()
    {
    }

    /**
     * @dataProvider convertProvider
     */
    public function testConvert($revenueCost, $input, $output)
    {
        $converter = new UmowaODzieloConverter($revenueCost);

        $this->assertEquals($output, $converter->convert($input));
    }

    public function convertProvider()
    {
        return array(
            array(20, 1600, 1370),
            array(20, 2000, 1712),
            array(50, 3500, 3185),
            array(50, 13000, 11830),
        );
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Revenue cost should be 20 or 50, 10 given.
     */
    public function testConverterThrowsExceptionIfNotProperRevenuePercent()
    {
        $converter = new UmowaODzieloConverter(10);
    }
}
