<?php

namespace GPS\ReportsBundle\Tests\Unit\Utils\Calculators\Salary;

$loader = require_once __DIR__.'/../../../../../../../../app/bootstrap.php.cache';

use GPS\ReportsBundle\Entity\JobConditions;
use GPS\ReportsBundle\Utils\Calculators\Salary\Salary;
use PHPUnit_Framework_TestCase as TestCase;
use \Mockery as m;

/**
 * Class SalaryGrossTest
 *
 * @package GPS\ReportsBundle\Tests\Entity
 */
class SalaryGrossTest extends TestCase
{

    public function setUp()
    {
        $this->mock = m::mock(new JobConditions());
        $this->mock->shouldReceive('getSalaryType')->andReturn(JobConditions::SALARY_GROSS);
        $this->mock->shouldReceive('getJobAgreement')->andReturn(JobConditions::AGREEMENT_JOB);

    }

    public function tearDown()
    {
        m::close();
    }

    public function testForGrossValueReturnsGrossResult()
    {
        $input = 1600;
        $this->mock->shouldReceive('getSalary')->andReturn($input);
        $this->mock->shouldReceive('getWorkTime')->andReturn(JobConditions::WORK_TIME_FULL);

        $salary = new Salary($this->mock);

        $this->assertEquals($input, $salary->getGross());
    }

    public function testHalfTimeWorkReturnsFullTimeValue()
    {
        $this->mock->shouldReceive('getSalary')->andReturn(1600);
        $this->mock->shouldReceive('getWorkTime')->andReturn(JobConditions::WORK_TIME_HALF);

        $expected = 3200;
        $salary = new Salary($this->mock);
        $this->assertEquals($expected, $salary->getGross());
    }

    /**
     * @dataProvider customWorkTimeReturnsFullTimeValueProvider
     */
    public function testCustomWorkTimeReturnsFullTimeValue($input, $workTime, $counter, $denominator, $expected)
    {
        $this->mock->shouldReceive('getSalary')->andReturn($input);
        $this->mock->shouldReceive('getWorkTime')->andReturn($workTime);

        $salary = new Salary($this->mock);
        $salary->setPartTimeFactor($counter, $denominator);

        $this->assertEquals($expected, $salary->getGross());
    }

    public function customWorkTimeReturnsFullTimeValueProvider()
    {
        return array(
            array(1600, JobConditions::WORK_TIME_PART, 1, 3, 4800),
            array(1200, JobConditions::WORK_TIME_PART, 4, 5, 1500),
            array(1800, JobConditions::WORK_TIME_PART, 5, 4, 1440),
        );
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Nor counter or denominator will be equal to 0
     */
    public function testZeroDenominatorThrowsException()
    {
        $this->mock->shouldReceive('getSalary')->andReturn(1600);
        $this->mock->shouldReceive('getWorkTime')->andReturn(JobConditions::WORK_TIME_PART);

        $salary = new Salary($this->mock);
        $salary->setPartTimeFactor(1, 0);
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Nor counter or denominator will be equal to 0
     */
    public function testZeroCounterThrowsException()
    {
        $this->mock = m::mock(new JobConditions());
        $this->mock->shouldReceive('getSalary')->andReturn(1600);
        $this->mock->shouldReceive('getWorkTime')->andReturn(JobConditions::WORK_TIME_PART);

        $salary = new Salary($this->mock);
        $salary->setPartTimeFactor(0, 1);
    }


    public function testForNetValueReturnsGrossResult()
    {
        $this->mock = m::mock(new JobConditions());
        $this->mock->shouldReceive('getSalary')->andReturn(2157);
        $this->mock->shouldReceive('getWorkTime')->andReturn(JobConditions::WORK_TIME_FULL);
        $this->mock->shouldReceive('getSalaryType')->andReturn(JobConditions::SALARY_NET);

        $salary = new Salary($this->mock);
        $this->assertEquals(3001, $salary->getGross());
    }

    /**
     * @dataProvider getGrossEstimationProvider
     */
    public function testGetGrossEstimation($input, $expected)
    {
        $this->mock = m::mock(new JobConditions());
        $this->mock->shouldReceive('getSalary')->andReturn(2157);
        $this->mock->shouldReceive('getWorkTime')->andReturn(JobConditions::WORK_TIME_FULL);
        $this->mock->shouldReceive('getSalaryType')->andReturn(JobConditions::SALARY_NET);

        $salary = new Salary($this->mock);
        $this->assertEquals($expected, $salary->getGrossEstimation($input));
    }

    public function getGrossEstimationProvider()
    {
        return array(
            array(1459, 2000),
            array(3550, 4998),
            array(7030, 9989),
            array(8312, 12015),
            array(10235, 14952),
            array(18085, 26940),
        );
    }
}
