<?php

namespace GPS\ReportsBundle\Tests\Unit\Utils\Calculators\Salary;

$loader = require_once __DIR__.'/../../../../../../../../app/bootstrap.php.cache';

use GPS\ReportsBundle\Entity\JobConditions;

use GPS\ReportsBundle\Utils\Calculators\Salary\GrossToNetConverter;
use GPS\ReportsBundle\Utils\Calculators\Salary\SelfEmployedConverter;
use GPS\ReportsBundle\Utils\Calculators\Salary\UmowaODzieloConverter;
use GPS\ReportsBundle\Utils\Calculators\Salary\UmowaOPraceConverter;
use GPS\ReportsBundle\Utils\Calculators\Salary\UmowaZlecenieConverter;
use PHPUnit_Framework_TestCase as TestCase;

/**
 * Class SalaryNetTest
 *
 * @package GPS\ReportsBundle\Tests\Entity
 */
class GrossToNetConverterTest extends TestCase
{

    public function setUp()
    {
    }

    /**
     * @dataProvider instanceProvider
     */
    public function testConverterIsProperClassInstance($agreementType, $className)
    {
        $param = array(
            'ownershipPeriod' => 1,
            'taxType' => 1,
            'insuranceType' => 2,
            'revenueCost' => 20,
        );
        $converter = new GrossToNetConverter($agreementType, $param);

        $this->assertInstanceOf($className, $converter->converter);
    }

    public function instanceProvider()
    {
        return array(
            array(JobConditions::AGREEMENT_JOB, get_class(new UmowaOPraceConverter())),
            array(JobConditions::AGREEMENT_UMOWA_O_DZIELO, get_class(new UmowaODzieloConverter(20))),
            array(JobConditions::AGREEMENT_UMOWA_ZLECENIE, get_class(new UmowaZlecenieConverter(1))),
            array(JobConditions::AGREEMENT_SELF_EMPLOYED, get_class(new SelfEmployedConverter(1, 1))),
        );
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Converter for agreement type [5] is not defined.
     */
    public function testConverterThrowsExceptionIfNotProperAgreementType()
    {
        $converter = new GrossToNetConverter(5);
    }
}
