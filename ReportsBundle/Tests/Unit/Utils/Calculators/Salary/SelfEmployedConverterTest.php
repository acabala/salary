<?php

namespace GPS\ReportsBundle\Tests\Unit\Utils\Calculators\Salary;

$loader = require_once __DIR__.'/../../../../../../../../app/bootstrap.php.cache';


use GPS\ReportsBundle\Utils\Calculators\Salary\SelfEmployedConverter;
use PHPUnit_Framework_TestCase as TestCase;

/**
 * Class SalaryNetTest
 *
 * @package GPS\ReportsBundle\Tests\Entity
 */
class SelfEmployedConverterTest extends TestCase
{

    public function setUp()
    {
    }

    /**
     * @dataProvider convertConvertLinearTaxProvider
     */
    public function testConvertLinearTax($ownershipPeriod, $input, $output)
    {
        $converter = new SelfEmployedConverter($ownershipPeriod, 1);

        $this->assertEquals($output, $converter->convert($input));
    }

    public function convertConvertLinearTaxProvider()
    {
        return array(
            array(2, 3000, 2531), // poniżej 2 lat
            array(2, 5400, 4475), // poniżej 2 lat
            array(1, 3000, 2036), // powyżej 2 lat
            array(1, 5400, 3980), // powyżej 2 lat
        );
    }

    /**
     * @dataProvider convertProgresiveTaxProvider
     */
    public function testConvertProgresiveTax($ownershipPeriod, $input, $output)
    {
        $converter = new SelfEmployedConverter($ownershipPeriod, 2);

        $this->assertEquals($output, $converter->convert($input));
    }

    public function convertProgresiveTaxProvider()
    {
        return array(
            array(1, 3000, 1973), // powyżej 2 lat, 1 próg podatkowy
            array(2, 3000, 2585), // poniżej 2 lat, 1 próg podatkowy
            array(1, 5400, 4320), // powyżej 2 lat, 1 próg podatkowy
            array(2, 5400, 4822), // poniżej 2 lat, 1 próg podatkowy
            array(1, 12000, 9203), // powyżej 2 lat, 2 próg podatkowy
        );
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Tax type should be 1 or 2, 3 given.
     */
    public function testConverterThrowsExceptionIfNotProperTaxType()
    {
        $converter = new SelfEmployedConverter(1, 3);
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Ownership period should be 1 or 2, 3 given.
     */
    public function testConverterThrowsExceptionIfNotProperOwnershipPeriod()
    {
        $converter = new SelfEmployedConverter(3, 1);
    }
}
