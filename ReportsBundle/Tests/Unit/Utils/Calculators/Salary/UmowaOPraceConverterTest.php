<?php

namespace GPS\ReportsBundle\Tests\Unit\Utils\Calculators\Salary;

$loader = require_once __DIR__.'/../../../../../../../../app/bootstrap.php.cache';

use GPS\ReportsBundle\Entity\JobConditions;

use GPS\ReportsBundle\Utils\Calculators\Salary\UmowaOPraceConverter;
use PHPUnit_Framework_TestCase as TestCase;

/**
 * Class SalaryNetTest
 *
 * @package GPS\ReportsBundle\Tests\Entity
 */
class UmowaOPraceConverterTest extends TestCase
{

    public function setUp()
    {
    }

    /**
     * @dataProvider convertProvider
     */
    public function testConvert($input, $output)
    {
        $converter = new UmowaOPraceConverter();

        $this->assertEquals($output, $converter->convert($input));
    }

    public function convertProvider()
    {
        return array(
            array(1600, 1181),
            array(2000, 1460),
            array(3500, 2505),
            array(13000, 9125),
        );
    }
}
