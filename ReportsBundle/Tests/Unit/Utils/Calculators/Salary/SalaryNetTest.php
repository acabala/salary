<?php

namespace GPS\ReportsBundle\Tests\Unit\Utils\Calculators\Salary;

$loader = require_once __DIR__.'/../../../../../../../../app/bootstrap.php.cache';

use GPS\ReportsBundle\Entity\JobConditions;
use GPS\ReportsBundle\Utils\Calculators\Salary\Salary;
use PHPUnit_Framework_TestCase as TestCase;
use \Mockery as m;

/**
 * Class SalaryNetTest
 *
 * @package GPS\ReportsBundle\Tests\Entity
 */
class SalaryNetTest extends TestCase
{

    public function setUp()
    {
        $this->mock = m::mock(new JobConditions());
        $this->mock->shouldReceive('getSalaryType')->andReturn(JobConditions::SALARY_NET);
        $this->mock->shouldReceive('getJobAgreement')->andReturn(JobConditions::AGREEMENT_JOB);
    }

    public function tearDown()
    {
        m::close();
    }


    public function testForNetValueReturnsNetResult()
    {
        $input = 1600;
        $this->mock->shouldReceive('getSalary')->andReturn($input);
        $this->mock->shouldReceive('getWorkTime')->andReturn(JobConditions::WORK_TIME_FULL);

        $salary = new Salary($this->mock);

        $this->assertEquals($input, $salary->getNet());
    }

    public function testHalfTimeWorkReturnsFullTimeValue()
    {
        $input = 1600;
        $expected = 3200;
        $this->mock->shouldReceive('getSalary')->andReturn($input);
        $this->mock->shouldReceive('getWorkTime')->andReturn(JobConditions::WORK_TIME_HALF);
        $salary = new Salary($this->mock);
        $this->assertEquals($expected, $salary->getNet());
    }

    /**
     * @dataProvider customWorkTimeReturnsFullTimeValueProvider
     */
    public function testCustomWorkTimeReturnsFullTimeValue($input, $workTime, $counter, $denominator, $expected)
    {
        $workTime = JobConditions::WORK_TIME_PART;
        $this->mock->shouldReceive('getSalary')->andReturn($input);
        $this->mock->shouldReceive('getWorkTime')->andReturn(JobConditions::WORK_TIME_PART);
        $salary = new Salary($this->mock);
        $salary->setPartTimeFactor($counter, $denominator);

        $this->assertEquals($expected, $salary->getNet());
    }

    public function customWorkTimeReturnsFullTimeValueProvider()
    {
        return array(
            array(1600, JobConditions::WORK_TIME_PART, 1, 3, 4800),
            array(1200, JobConditions::WORK_TIME_PART, 4, 5, 1500),
            array(1800, JobConditions::WORK_TIME_PART, 5, 4, 1440),
        );
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Nor counter or denominator will be equal to 0
     */
    public function testZeroDenominatorThrowsException()
    {
        $input = 100;
        $this->mock->shouldReceive('getSalary')->andReturn($input);
        $this->mock->shouldReceive('getWorkTime')->andReturn(JobConditions::WORK_TIME_PART);
        $salary = new Salary($this->mock);
        $salary->setPartTimeFactor(1, 0);
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Nor counter or denominator will be equal to 0
     */
    public function testZeroCounterThrowsException()
    {
        $input = 100;
        $this->mock->shouldReceive('getSalary')->andReturn($input);
        $this->mock->shouldReceive('getWorkTime')->andReturn(JobConditions::WORK_TIME_PART);
        $salary = new Salary($this->mock);
        $salary->setPartTimeFactor(0, 1);
    }

    public function testForGrossValueReturnsNetResult()
    {

        $mock = m::mock(new JobConditions());
        $mock->shouldReceive('getSalary')->andReturn(2000);
        $mock->shouldReceive('getWorkTime')->andReturn(JobConditions::WORK_TIME_FULL);
        $mock->shouldReceive('getJobAgreement')->andReturn(JobConditions::AGREEMENT_JOB);
        $mock->shouldReceive('getSalaryType')->andReturn(JobConditions::SALARY_GROSS);

        $salary = new Salary($mock);
        $this->assertEquals(1460, $salary->getNet());
    }
}
