<?php

namespace GPS\ReportsBundle\Tests\Unit\Utils\Calculators\Salary;

$loader = require_once __DIR__.'/../../../../../../../../app/bootstrap.php.cache';

use GPS\ReportsBundle\Entity\JobConditions;

use GPS\ReportsBundle\Utils\Calculators\Salary\UmowaZlecenieConverter;
use PHPUnit_Framework_TestCase as TestCase;

/**
 * Class SalaryNetTest
 *
 * @package GPS\ReportsBundle\Tests\Entity
 */
class UmowaZlecenieConverterTest extends TestCase
{

    public function setUp()
    {
    }

    /**
     * @dataProvider convertInsuranceOnlyEmployerProvider
     */
    public function testConvertInsuranceOnlyEmployer($insuranceType, $input, $output)
    {
        $converter = new UmowaZlecenieConverter($insuranceType);

        $this->assertEquals($output, $converter->convert($input));
    }

    public function convertInsuranceOnlyEmployerProvider()
    {
        return array(
            array(1, 1600, 1165),
            array(1, 2000, 1456),
        );
    }

    /**
     * @dataProvider convertInsuranceAnotherEmployerProvider
     */
    public function testConvertInsuranceAnotherEmployer($insuranceType, $input, $output)
    {
        $converter = new UmowaZlecenieConverter($insuranceType);

        $this->assertEquals($output, $converter->convert($input));
    }

    public function convertInsuranceAnotherEmployerProvider()
    {
        return array(
            array(2, 1600, 1350),
            array(2, 2000, 1687),
        );
    }

    /**
     * @dataProvider convertInsuranceStudentProvider
     */
    public function testConvertInsuranceStudent($insuranceType, $input, $output)
    {
        $converter = new UmowaZlecenieConverter($insuranceType);

        $this->assertEquals($output, $converter->convert($input));
    }

    public function convertInsuranceStudentProvider()
    {
        return array(
            array(3, 1600, 1370),
            array(3, 2000, 1712),
        );
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Insurance type should be 1, 2 or 3, 4 given.
     */
    public function testConverterThrowsExceptionIfNotProperRevenuePercent()
    {
        $converter = new UmowaZlecenieConverter(4);
    }
}
