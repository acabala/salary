<?php
/**
 * Created by PhpStorm.
 * User: a.cabala
 * Date: 02.01.14
 * Time: 13:35
 */

namespace GPS\ReportsBundle\Utils\Calculators\Salary;


class UmowaZlecenieConverter implements GrossToNetConverterInterface {

    const ZUS_PERCENT = 13.71;
    const TAX_PERCENT = 18;
    const KOSZTY_PRZYCHODU_PERCENT = 20;
    const UBEZPIECZENIE_ZDROWOTNE_A = 9;
    const UBEZPIECZENIE_ZDROWOTNE_B = 7.75;

    protected $insuranceType;

    public function __construct($insuranceType)
    {
        $this->setInsuranceType($insuranceType);
    }

    public function convert($gross)
    {

        switch ($this->insuranceType) {
            case 1:// jedyny tytuł ubezpieczenia
                $net = $this->convertInsuranceOnlyEmployer($gross);
                break;
            case 2: // ubezpieczenie u innego pracodawcy
                $net = $this->convertInsuranceAnotherEmployer($gross);
                break;
            case 3: // ubezpieczenie studenta
                $net = $this->convertInsuranceStudent($gross);
                break;
        }

        return $net;
    }

    protected function setInsuranceType($type)
    {
        if ($type == 1 || $type == 2 || $type == 3) {
            $this->insuranceType = $type;
        } else {
            throw new \Exception("Insurance type should be 1, 2 or 3, {$type} given.");
        }
    }

    /**
     * Przeliczanie kwoty netto brutto na netto dla jednego pracodawcy
     *
     * Algorytm uwzględnia odliczanie składkę ZUS i ubezpieczenie chorobowe     *
     * @param $gross Kwota brutto
     * @return float przeliczona kwota netto
     */
    protected function convertInsuranceOnlyEmployer($gross)
    {
        $baseAmount = $gross - round( $gross * self::ZUS_PERCENT / 100);
        $revenueCost = round($baseAmount * self::KOSZTY_PRZYCHODU_PERCENT / 100);
        $basisOfTax = round(($baseAmount - $revenueCost) * self::TAX_PERCENT / 100);
        $tax = round($basisOfTax - round($baseAmount * self::UBEZPIECZENIE_ZDROWOTNE_B / 100));
        $net = round($baseAmount - $tax - round($baseAmount * self::UBEZPIECZENIE_ZDROWOTNE_A / 100));

        return $net;
    }

    protected function convertInsuranceAnotherEmployer($gross)
    {
        $revenueCost = round($gross * self::KOSZTY_PRZYCHODU_PERCENT / 100);
        $basisOfTax = round(($gross - $revenueCost) * self::TAX_PERCENT / 100);
        $tax = round($basisOfTax - round($gross * self::UBEZPIECZENIE_ZDROWOTNE_B / 100));
        $net = round($gross - $tax - round($gross * self::UBEZPIECZENIE_ZDROWOTNE_A / 100));

        return $net;
    }

    public function convertInsuranceStudent($gross)
    {
        $revenueCost = round($gross * self::KOSZTY_PRZYCHODU_PERCENT / 100);
        $basisOfTax = $gross - $revenueCost;
        $tax = $basisOfTax * self::TAX_PERCENT / 100;
        $net = round($gross - $tax);

        return $net;
    }
} 