<?php
namespace GPS\ReportsBundle\Utils\Calculators\Salary;


use GPS\ReportsBundle\Entity\JobConditions;

class Salary {

    const GROSS_ESTIMATION_BREAK_POINT = 11000;

    protected $isAmountNormalized = false;

    protected $amount;

    protected $partTimeFactor;

    protected $workTime;

    protected $salaryType;

    protected $jobAgreement;

    protected $converterParams = array();

    /**
     * @param $amount podstawowa dana z bazy do przeliczeń
     * @param $workTime rodzaj czasu pracy
     * @param $salaryType typ salary: netto/brutto
     */
    public function __construct(JobConditions $jobConditions)
    {
       $this->setJobConditionsParamneters($jobConditions);
    }

    protected function setJobConditionsParamneters(JobConditions $jobConditions)
    {
        $this->setAmount($jobConditions);
        $this->workTime = $jobConditions->getWorkTime();
        $this->salaryType = $jobConditions->getSalaryType();
        $this->jobAgreement = $jobConditions->getJobAgreement();

        if ($this->salaryType == JobConditions::SALARY_GROSS) {
            $this->setConverterParams($jobConditions);
        }
    }

    protected function setAmount(JobConditions $jobConditions)
    {
        $this->amount = $jobConditions->getSalary();
    }

    protected function setConverterParams(JobConditions $jobConditions)
    {
        switch ($this->jobAgreement) {
            case JobConditions::AGREEMENT_SELF_EMPLOYED:
                $this->converterParams['ownershipPeriod'] = $jobConditions->getOwnBusinessPeriod();
                $this->converterParams['taxType'] = $jobConditions->getOwnBusinessTax();
                break;
            case JobConditions::AGREEMENT_UMOWA_ZLECENIE:
                $this->converterParams['insuranceType'] = $jobConditions->getMandateInsuranceType();
                break;
            case JobConditions::AGREEMENT_UMOWA_O_DZIELO:
                $this->converterParams['revenueCost'] = $jobConditions->getTaxDeductibleExpenses();
                break;
        }
    }

    protected function normalizeAmount($amount, $workTime)
    {
        if (!$this->isAmountNormalized) {
            if ($workTime == JobConditions::WORK_TIME_FULL) {
                $this->setPartTimeFactor(1, 1);
            } else if ($workTime == JobConditions::WORK_TIME_HALF) {
                $this->setPartTimeFactor(1, 2);
            }
            $amount = $amount * $this->getPartTimeFactor();

            $this->amount = $amount;
            $this->isAmountNormalized = true;
        }
    }

    public function getPartTimeFactor()
    {
        return $this->partTimeFactor;
    }

    public function setPartTimeFactor($counter, $denominator)
    {
        if ($counter == 0 || $denominator == 0) {
            throw new \Exception('Nor counter or denominator will be equal to 0');
        }
        $this->partTimeFactor = $denominator / $counter;
    }


    /**
     * Zwraca wartość netto pensji
     *
     * @return float
     */
    public function getNet()
    {
        $this->normalizeAmount($this->amount, $this->workTime);
        $amount = $this->amount;

        if ($this->salaryType == JobConditions::SALARY_GROSS) {
            $converter = new GrossToNetConverter($this->jobAgreement, $this->converterParams);
            $amount = $converter->convert($amount);
        }

        return $amount;
    }

    /**
     * Zwraca wartość brutto pensji
     *
     * @return float
     */
    public function getGross()
    {
        $this->normalizeAmount($this->amount, $this->workTime);
        if ($this->salaryType == JobConditions::SALARY_GROSS) {
            $amount = $this->amount;
        } else {
            $amount = $this->getGrossEstimation($this->amount);
        }
        return $amount;
    }

    /**
     * Estymowanie wartości brutto wynagrodzenia dla podanej wartości netto.
     *
     * Estymacja jest przeprowadzania w oparciu o 2 funkcje liniowe, z punktem zmiany w GROSS_ESTIMATION_BREAK_POINT.
     * Dane są zbliżone do dokładnych przeliczeń.
     *
     * @param $netAmount wartość netto
     * @return int
     */
    public function getGrossEstimation($netAmount)
    {
        $estimated = 1.434 * $netAmount - 92.424;
        if ($estimated >= Salary::GROSS_ESTIMATION_BREAK_POINT) {
            $estimated = 1.5272 * $netAmount - 679.31;
        }

        return round($estimated, 0);
    }

} 