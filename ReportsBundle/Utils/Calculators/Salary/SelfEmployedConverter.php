<?php
/**
 * Created by PhpStorm.
 * User: a.cabala
 * Date: 02.01.14
 * Time: 13:35
 */

namespace GPS\ReportsBundle\Utils\Calculators\Salary;


class SelfEmployedConverter implements GrossToNetConverterInterface {

    const ZUS_COLLECTION_LESS_THAN_2_YEARS = 153.22;
    const ZUS_COLLECTION_MORE_THAN_2_YEARS = 765.25;
    const TAX_FIRST_STEP = 85528;
    const TAX_FREE_AMOUNT = 556.02;

    protected $ownershipPeriod;

    protected $taxType;

    public function __construct($ownershipPeriod, $taxType)
    {
        $this->setOwnershipPeriod($ownershipPeriod);
        $this->setTaxType($taxType);
    }

    public function convert($gross)
    {
        $zusCollection = $this->getZusCollection();
        if ($this->taxType == 1) {
            $net = $this->convertWithLinearTax($gross);
        } else if ($this->taxType == 2) {
            $net = $this->convertWithProgresiveTax($gross);
        }

        return $net;
    }

    protected function getZusCollection()
    {
        if ($this->ownershipPeriod == 1) {
            $collection = self::ZUS_COLLECTION_MORE_THAN_2_YEARS;
        } else if ($this->ownershipPeriod == 2) {
            $collection = self::ZUS_COLLECTION_LESS_THAN_2_YEARS;
        }

        return $collection;
    }

    protected function setOwnershipPeriod($period)
    {
        if ($period == 1 || $period == 2) {
            $this->ownershipPeriod = $period;
        } else {
            throw new \Exception("Ownership period should be 1 or 2, {$period} given.");
        }
    }

    protected function setTaxType($type)
    {
        if ($type == 1 || $type == 2) {
            $this->taxType = $type;
        } else {
            throw new \Exception("Tax type should be 1 or 2, {$type} given.");
        }
    }

    protected function convertWithLinearTax($gross)
    {
        $baseAmount = $gross - $this->getZusCollection();
        $tax = $baseAmount * 19 / 100 - 225.38;
        $net = round($baseAmount - $tax);

        return $net;
    }

    protected function convertWithProgresiveTax($gross)
    {
        $grossYearly = $gross * 12;
        $zusYearly = $this->getZusCollection() * 12;
        $baseOfTax = $grossYearly - $zusYearly;

        if ($baseOfTax < self::TAX_FIRST_STEP) {
            $taxYearly = $baseOfTax * 18 / 100 - (self::TAX_FREE_AMOUNT + 225.38 ) * 12 ;
        } else {
            $taxYearly = 14839.02 + ($baseOfTax - self::TAX_FIRST_STEP) * 32 / 100
                - (self::TAX_FREE_AMOUNT + 225.38) * 12;
        }
        $taxYearly = round($taxYearly);
        $taxYearly = ($taxYearly < 0) ? 0 : $taxYearly;

        $net = round(($baseOfTax - $taxYearly - 261.73 * 12) / 12);

        return $net;
    }
}