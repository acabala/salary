<?php
namespace GPS\ReportsBundle\Utils\Calculators\Salary;


use GPS\ReportsBundle\Entity\JobConditions;
use GPS\ReportsBundle\Utils\Calculators\Salary\Salary;

class ExpectedSalary extends Salary {

    protected function setAmount(JobConditions $jobConditions)
    {
        $this->amount = $jobConditions->getExpectedSalary();
    }

} 