<?php
/**
 * Created by PhpStorm.
 * User: a.cabala
 * Date: 02.01.14
 * Time: 13:35
 */

namespace GPS\ReportsBundle\Utils\Calculators\Salary;


use GPS\ReportsBundle\Entity\JobConditions;

class GrossToNetConverter implements GrossToNetConverterInterface
{

    public function __construct($agreement, $param = array())
    {
        switch ($agreement) {
            case JobConditions::AGREEMENT_JOB:
                $this->converter =  new UmowaOPraceConverter();
                break;
            case JobConditions::AGREEMENT_SELF_EMPLOYED:
                $this->converter =  new SelfEmployedConverter($param['ownershipPeriod'], $param['taxType']);
                break;
            case JobConditions::AGREEMENT_UMOWA_ZLECENIE:
                $this->converter =  new UmowaZlecenieConverter($param['insuranceType']);
                break;
            case JobConditions::AGREEMENT_UMOWA_O_DZIELO:
                $this->converter =  new UmowaODzieloConverter($param['revenueCost']);
                break;
            default:
                throw  new \Exception('Converter for agreement type [' . $agreement . '] is not defined.');
        }

    }

    public function convert($gross)
    {
        return $this->converter->convert($gross);
    }
} 