<?php
/**
 * Created by PhpStorm.
 * User: a.cabala
 * Date: 02.01.14
 * Time: 13:35
 */

namespace GPS\ReportsBundle\Utils\Calculators\Salary;


class UmowaODzieloConverter implements GrossToNetConverterInterface {

    const TAX_PERCENT = 18;
    protected $revenueCost;

    public function __construct($revenueCost)
    {
        $this->setRevenueCost($revenueCost);
    }

    public function convert($gross)
    {
        $revenueExpenses = $gross - ($gross * $this->revenueCost / 100);
        $tax = round($revenueExpenses * self::TAX_PERCENT / 100);
        $net = $gross - $tax;

        return $net;
    }

    protected function setRevenueCost($revenueCost)
    {
        if ($revenueCost == 20 || $revenueCost == 50) {
        $this->revenueCost = $revenueCost;
    } else {
        throw new \Exception("Revenue cost should be 20 or 50, {$revenueCost} given.");
    }
    }
} 