<?php
/**
 * Created by PhpStorm.
 * User: a.cabala
 * Date: 02.01.14
 * Time: 13:35
 */

namespace GPS\ReportsBundle\Utils\Calculators\Salary;


interface GrossToNetConverterInterface {

    public function convert($gross);
}
