<?php
/**
 * Created by PhpStorm.
 * User: a.cabala
 * Date: 02.01.14
 * Time: 13:35
 */

namespace GPS\ReportsBundle\Utils\Calculators\Salary;


class UmowaOPraceConverter implements GrossToNetConverterInterface {

    const ZUS_PERCENT = 13.71;
    const TAX_PERCENT = 18;
    const KOSZTY_PRZYCHODU = 111.25;
    const KWOTA_WOLNA = 46.34;
    const UBEZPIECZENIE_ZDROWOTNE_A = 9;
    const UBEZPIECZENIE_ZDROWOTNE_B = 7.75;

    public function convert($gross)
    {
        $zusTaxes = round($gross * self::ZUS_PERCENT / 100, 2);
        $baseAmount = $gross - $zusTaxes;

        $tmp = $baseAmount - self::KOSZTY_PRZYCHODU;
        $tmp = round($tmp * self::TAX_PERCENT / 100, 2);

        $preTax = $tmp - self::KWOTA_WOLNA;
        $healthTaxA = round($baseAmount * self::UBEZPIECZENIE_ZDROWOTNE_A / 100, 2);
        $healthTaxB = round($baseAmount * self::UBEZPIECZENIE_ZDROWOTNE_B / 100, 2);

        $tax = round($preTax - $healthTaxB, 2);

        $net = round($baseAmount - $healthTaxA - $tax);

        return $net;
    }
} 