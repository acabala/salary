<?php
namespace GPS\ReportsBundle\Utils\Calculators\Salary;


use GPS\ReportsBundle\Entity\JobConditions;
use GPS\ReportsBundle\Utils\Calculators\Salary\Salary;

class TotalSalary extends Salary {

    protected function setAmount(JobConditions $jobConditions)
    {
        $this->amount = $jobConditions->getCurrentSalary();
        if ($bonus = $jobConditions->getBonuses()->first()) {
            $this->amount += $bonus->getNormalizedAmount();
        }
    }

} 